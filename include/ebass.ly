ebass = \relative c, {
	\set Staff.instrumentName = "Electric Bass"

	%% A %%
	\repeat percent 4 { c4 c4 c4 c4 }
	%% B %%
	\repeat percent 4 { f4 f4 f4 f4 }
}
