\version "2.18.2"

\paper {
	#(set-paper-size "a4")          % paper size
	% ragged-last-bottom = ##f      % fill the page till bottom
	% page-count = 1                % force to render just in 1 page
	% between-system-padding = 0    % empty padding between lines
}

NoChords = {
	\override Score.MetronomeMark #'padding = #4
}
WithChords = {
	\override Score.MetronomeMark #'padding = #8
}

\header {
	title = \markup \center-align {
		\override #'(font-name . "Purisa")
		\fontsize #4 \bold
		"Higher Ground"
	}
	subsubtitle = \markup \center-align {
		\override #'(font-name . "Arial")
		"Dave Weckl version"
	}
	composer=\markup \center-align {
		\override #'(font-name . "Arial")
		"Stevie Wonder"
	}
	opus = \markup \tiny {
		\override #'(font-name . "Arial")
		"(1973)"
	}
	tagline = \markup {
		\override #'(font-name . "Arial")
		\tiny \column {
			\fill-line { "Transcription" \italic { "Giuseppe Ricupero" } }
			\fill-line { "Updated" \italic { "06-11-2018 20.12" } }
		}
	}
}

global = {
	\time 4/4
	\key ees \minor
	\tempo 4 = 124
	\set Score.skipBars = ##t
	\set Score.countPercentRepeats = ##t
}
